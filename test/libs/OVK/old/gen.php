<?php
include('base58.php');
include('pemconv.php');

function genPair() {
	$config = array(
		'private_key_bits' => 1024,
//		'private_key_type' => OPENSSL_KEYTYPE_DSA
	);
	$keyRes = openssl_pkey_new($config);
	openssl_pkey_export($keyRes, $privatePem);
	$privateBin = pemconv::pem2bin($privatePem);

	$keyInfo = openssl_pkey_get_details($keyRes);
	$publicPem = $keyInfo['key'];
	var_dump($publicPem);
	$publicBin = pemconv::pem2bin($publicPem);

	$priv58 = base58::encode($privateBin);
	$pub58 = base58::encode($publicBin);

	return array(
		'private'=>$privateBin,
		'private58'=>$priv58,
		'public'=>$publicBin,
		'public58'=>$pub58
	);
}

function buildMulti($proofs_needed,$pubs) { //send a list of pub keys
	$redeem = chr($proofs_needed);
	foreach($pubs as $pub) $redeem .= $pub;
	return array(
		'redeem'=>$redeem,
		'check'=>md5($redeem),
		'address'=>base58::encode(pack('H*',hash('sha256',$redeem))),
		'redeem58'=>base58::encode($redeem)
	);

}

//Pairs are in base58 
$one = genPair();
$two = genPair();
$three = genPair();

$multi = buildMulti( 2, array(
		$one['public'],
		$two['public'],
		$three['public'],
	));

echo "Your multiaddress is : {$multi['address']}\n";
echo "Your redeem code is : {$multi['redeem58']}\n";
echo "Your redeem check code is : {$multi['check']}\n";
echo "Your key pairs : \n";

echo "\tOne :\n";
echo "\t\t Private : {$one['private58']}\n";
echo "\t\t Public : {$one['public58']}\n";

echo "\tTwo :\n";
echo "\t\t Private : {$two['private58']}\n";
echo "\t\t Public : {$two['public58']}\n";

echo "\tThree :\n";
echo "\t\t Private : {$three['private58']}\n";
echo "\t\t Public : {$three['public58']}\n";


?>
