<?php
class pemconv {
	public static function pem2bin($pem) {
		$pemParts = split("\n",$pem);
		$dechunked = "";
		foreach($pemParts as $line) if (substr($line,0,4) != "----") $dechunked .= trim($line);
		return base64_decode($dechunked);
	}

	public static function bin2pem($bin,$private=false) {
		if ($private) {
			return "-----BEGIN PRIVATE KEY-----\n".chunk_split(base64_encode($bin),64,"\n")."-----END PRIVATE KEY-----\n";
		} else {
			return "-----BEGIN PUBLIC KEY-----\n".chunk_split(base64_encode($bin),64,"\n")."-----END PUBLIC KEY-----\n";
		}
	}
}
?>
