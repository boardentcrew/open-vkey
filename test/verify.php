#!/usr/bin/php -q
<?php
	include('ovk.php');
	if (!$argv[1]) die("Verify what?\n");
	$file = $argv[1];
	$contents = file_get_contents($file);
	if (!($json = json_decode($contents))) {
		die("Unable to read file.\n");
	}

	if (!$data = json_decode($contents)) {
		die("Unable to parse OVK file.\n");
	}

	if ($signed = OVK::verify($data)) {
		echo "Verified, signed on ".date("r",$signed)."\n";
	} else {
		echo "Invalid";
	}
?>
