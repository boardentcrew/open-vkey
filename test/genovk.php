#!/usr/bin/php -q
<?php
include('ovk.php');

/*
* To create an open virtual card, you need 4 things. 
	1. An Address ( derrived from a tombkey + salt )
	2. A Profile block of information
	3. A timestamp of ovk creation
	4. A signature of the CRC32 of profile information with timestamp attached.
*/

//Ideal Setup
// $keys = OVK::genKeys();

$keys = OVK::genKeys();
$address = OVK::address($keys['public']);
$unique = substr($address,-4);

$profile = array(
	'name'=>'Dan Morgan',
	'avatar'=>'http://i.avatarfoundry.com/tl.jpg',
	'bio'=>'Hello, my name is danmorgan, and I am creating a new standard for authentication, called Open Virtual Keys',
	'website'=>'http://www.danmorgan.net'
);

$fp = fopen("ovks/{$unique}.key","w");
fputs($fp,json_encode(OVK::encodeKeys($keys),JSON_PRETTY_PRINT));
fclose($fp);

$fp = fopen("ovks/{$unique}.ovk","w");
fputs($fp,json_encode(OVK::buildOVK($profile,$keys), JSON_PRETTY_PRINT));
fclose($fp);

echo "Finished creating OVK : {$unique}\n";
?>
