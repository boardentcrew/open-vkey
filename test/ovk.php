<?php
//OVK - Facade for Open Virtual Key Functions
class OVK {
	
	static public function genKeys() {
		$keyRes = openssl_pkey_new(array(
			'private_key_bits'=>512
		));
		openssl_pkey_export($keyRes, $privatePem);
		$privateBin = self::pem2bin($privatePem);
		$keyInfo = openssl_pkey_get_details($keyRes);
		$publicPem = $keyInfo['key'];
		$publicBin = self::pem2bin($publicPem);
		return array('private'=>bin2hex($privateBin),'public'=>bin2hex($publicBin));
	}

	static public function buildOVK($profile,$keys) {
		$profile_crc = crc32(json_encode($profile));
		$creationTime = time();
		$lock = $profile_crc.$creationTime;
		$signature = static::sign($lock,$keys['private']);
		$arr = [
			'address'=>static::address($keys['public']),
			'badge'=>static::encode58($keys['public']),
			'signature'=>['signed'=>$signature,'date'=>$creationTime],
			'profile'=>$profile,
		];
		return $arr;
	}

	static public function encodeKeys($keys) {
		return array(
			'private'=>static::encode58($keys['private']),
			'public'=>static::encode58($keys['public'])
		);
	}

	static public function sign($content,$privateKey) {
		openssl_private_encrypt($content,$signature,static::bin2pem(static::hex2bin($privateKey),true));
		$encoded_signature = static::encode58(bin2hex($signature));
		return $encoded_signature;
	}

	static public function verify($ovk) {
	        $crc = crc32(json_encode($ovk->profile));
	        $hexBadge = static::decode58($ovk->badge);
	        $pem = static::bin2pem(static::hex2bin($hexBadge));
		openssl_public_decrypt(static::hex2bin(static::decode58($ovk->signature->signed)),$signature,static::bin2pem(static::hex2bin(static::decode58($ovk->badge))));
		
        	if ($crc.$ovk->signature->date == $signature) { //Signature does match badge
			if (static::address($ovk->badge) == $ovk->address) { //badge does match address
				return $ovk->signature->date;
			} else return false;
		} else {
			return false;
		}
        }

	static public function address($badge,$type='01') {
		return self::encode58($type.hash('sha256', hash('sha256', $badge)));
	}

	public static function pem2bin($pem) {
		$pemParts = split("\n",$pem);
		$dechunked = "";
		foreach($pemParts as $line) if (substr($line,0,4) != "----") $dechunked .= trim($line);
		return base64_decode($dechunked);
	}

	public static function bin2pem($bin,$private=false) {
		if ($private) {
			return "-----BEGIN PRIVATE KEY-----\n".chunk_split(base64_encode($bin),64,"\n")."-----END PRIVATE KEY-----\n";
		} else {
			return "-----BEGIN PUBLIC KEY-----\n".chunk_split(base64_encode($bin),64,"\n")."-----END PUBLIC KEY-----\n";
		}
	}

	public static function hex2bin($hex) {
		return pack('H*',$hex);
	}

	static public function encode58($addr) {
		$num = gmp_strval(gmp_init($addr, 16), 58);
		$num = strtr($num, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv', '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');
		return $num;
	}
	static public function decode58($addr) {
		$deshift = strtr($addr, '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz', '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv' );
		$num = gmp_strval(gmp_init($deshift, 58),16);
		return str_pad($num,(strlen($num)%2?strlen($num)+1:strlen($num)),'0', STR_PAD_LEFT);
	}
}
?>
