<?php
include("../common.php");
$overrides = OVK::newPair();
echo "Override Secret : ".OVK::encode58($overrides[0])."\n";
echo "Override Badge : ".OVK::encode58($overrides[1])."\n";

$idents = OVK::newPair();
echo "Ident Secret : ".OVK::encode58($idents[0])."\n";
echo "Ident Badge : ".OVK::encode58($idents[1])."\n";

$badge = $overrides[1].$overrides[1];
echo "Badge : ".OVK::encode58($badge)."\n";

$address = OVK::address($badge);
echo "Address : ".OVK::encode58($address)."\n";

openssl_private_encrypt('test',$crypted,OVK::bin2pem(pack('H*',$idents[0]),true));
echo "Crypted : ".OVK::encode58(bin2hex($crypted))."\n";

openssl_public_decrypt($crypted,$message,OVK::bin2pem(pack('H*',$idents[1])));
echo "Message : {$message}\n";
?>
