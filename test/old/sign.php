<?php
	include("../common.php");

	$private = $argv[1];
	$hash = $argv[2];
	$ecc = OVK::newEcc();

	$sig = join($ecc->signPoints($private,$hash),'');
	echo "Signature : ".OVK::encode58($sig)."\n";
?>
