<?php
//Our openvkey profile
$keys = 10;
$profile = array(
	'name'=>'Dan Morgan',
	'uuid'=>'abcdef-123',
	'avatar'=>'me.gif'
);

//convert into json
$data = json_encode($profile);

//Create a new cipher
$secureKey = hash('sha256',rand(0,999999),TRUE);
$iv = mcrypt_create_iv(32);
$payload = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $secureKey, $data, MCRYPT_MODE_ECB, $iv));
echo "secureKey: ".base64_encode($secureKey)."\n";
echo "payload : {$payload}\n\n";

//Build Key
$config = array(
	"digest_alg" => "sha512",
	"private_key_bits" => 384,
	"private_key_type" => OPENSSL_KEYTYPE_RSA,
);

$res = openssl_pkey_new($config);
openssl_pkey_export($res,$privKey);
$pubKey = openssl_pkey_get_details($res);
$pubKey = $pubKey['key'];

for($i=0;$i<$keys;$i++) $inKeys[] = openssl_get_publickey($pubKey);

echo "Private Key : ".base64_encode($privKey)."\n";;
openssl_seal($secureKey,$sealedKey,$ekeys,$inKeys);
foreach($ekeys as $k=>$binKey) {
	echo "Public Key #{$k} : ".base64_encode($binKey)."\n";
}
echo "Data : ".base64_encode($sealedKey);
echo "\n";
?>
