#!/usr/bin/php -q
<?php
include("../autoload.php");

//Resigns an OVK

if (!$ovkContents = json_decode(file_get_contents($argv[1]))) {
	die("Unable to read ovk file.\n");
}
if (!$keyContents = file_get_contents($argv[2])) {
	die("Unable to read tombkey file.\n");
}
$address = $ovkContents->address;
$badge = $ovkContents->badge;
$json_profile = $ovkContents->profile;
$profile_crc = crc32($json_profile);
$creationTime = time();
$lock = $profile_crc.$creationTime;
openssl_private_encrypt($lock,$signature,OVK::bin2pem(pack('H*',OVK::decode58($keyContents)),true));

$obj = [
	'address'=>$address,
	'badge'=>$badge,
	'signature'=>['signed'=>OVK::encode58(bin2hex($signature)),'date'=>$creationTime],
	'profile'=>$json_profile,
];


$fp = fopen($argv[1],"w");
fputs($fp,json_encode($obj, JSON_PRETTY_PRINT));
fclose($fp);

echo "Finished resigning OVK : {$argv[1]}\n";
?>
