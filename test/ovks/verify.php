#!/usr/bin/php -q
<?php
	include("../autoload.php");
	if (!$argv[1]) die("Verify what?\n");
	$file = $argv[1];
	$contents = file_get_contents($file);
	if (!($json = json_decode($contents))) {
		die("Unable to read file.\n");
	}

	if (!$data = json_decode($contents)) {
		die("Unable to parse OVK file.\n");
	}
	
	if (!$profile = json_decode($data->profile)) {
		die("Unable to read profile information from OVK file.\n");
	}

	$crc = crc32($data->profile);
	$hexBadge = OVK::decode58($data->badge);
	$pem = OVK::bin2pem(pack('H*',$hexBadge));
	openssl_public_decrypt(pack('H*',OVK::decode58($data->signature->signed)),$signature,OVK::bin2pem(pack('H*',OVK::decode58($data->badge))));
	if ($crc.$data->signature->date != $signature) {
		die("This OVK is not authentic.\n");
	}
	var_dump(json_decode($data->profile));
	echo "OVK is authentic. Signed on ".date("r",$data->signature->date);	
	echo "\n";
?>
