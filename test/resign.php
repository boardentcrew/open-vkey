#!/usr/bin/php -q
<?php
	include('ovk.php');
	
	switch (count($argv)) {
		case 0 :
		case 1 : echo "resign.php <file.ovk> <file.keys>\n"; break;
	}
	$contents = file_get_contents($argv[1]);

	if (!$ovk = json_decode($contents)) {
		die("Unable to parse OVK file.\n");
	}

	$contents = file_get_contents($argv[2]);

	if (!($keys = json_decode($contents))) {
		die("Unable to read keys file.\n");
	}

	$new = OVK::buildOVK($ovk->profile,$keys);

	$fp = fopen($argv[1],"w");
	fputs($fp,json_encode($new, JSON_PRETTY_PRINT));
	fclose($fp);

	echo "Resigned OVK : ".$ovk->address."\n";

?>
