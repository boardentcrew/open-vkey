<?php
include('base58.php');
include('pemconv.php');

function readin() {
	$fr = fopen("php://stdin","r");
	$input = fgets($fr,4068);
	fclose($fr);
	return trim($input);
}

echo "Private Key ? ";
$private58 = readin();
$privateBin = base58::decode($private58);
$privatePem = pemconv::bin2pem($privateBin,true);
$privKey = openssl_pkey_get_private($privatePem);

echo "Domain ? ";
$domain = readin();
openssl_private_encrypt( $domain, $crypt, $privKey );
$signature = base58::encode($crypt);
echo "Signature : {$signature}\n";

?>
