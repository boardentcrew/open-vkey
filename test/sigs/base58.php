<?php
class base58 {
	public static function encode($bin) {
		$num = self::bin2dec($bin);
		return self::arb_encode($num, '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');
	}

	public static function decode($num) {
		$num = self::arb_decode($num, '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');
		return self::dec2bin($num);
	}

	static function arb_encode($num, $basestr) {
		if( ! function_exists('bcadd') ) {
			Throw new Exception('You need the BCmath extension.');
		}
		$base = strlen($basestr);
		$rep = '';
		while( true ){
			if( strlen($num) < 2 ) {
				if( intval($num) <= 0 ) {
					break;
				}
			}
			$rem = bcmod($num, $base);
			$rep = $basestr[intval($rem)] . $rep;
			$num = bcdiv(bcsub($num, $rem), $base);
		}
		return $rep;
	}


	public static function arb_decode( $num, $basestr ) {
		if( ! function_exists('bcadd') ) {
			Throw new Exception('You need the BCmath extension.');
		}

		$base = strlen($basestr);
		$dec = '0';

		$num_arr = str_split((string)$num);
		$cnt = strlen($num);
		for($i=0; $i < $cnt; $i++) {
			$pos = strpos($basestr, $num_arr[$i]);
			if( $pos === false ) {
				Throw new Exception(sprintf('Unknown character %s at offset %d', $num_arr[$i], $i));
			}
			$dec = bcadd(bcmul($dec, $base), $pos);
		}
		return $dec;
	}

	static function dec2bin($decimal_i) {
		$hex = self::dec2hex($decimal_i);
		return pack("H*",$hex);
	}

	static function dec2hex($decimal_i) {
		$hex = '';
		do {
			$last = bcmod($decimal_i,16);
			$hex = dechex($last).$hex;
			$decimal_i = bcdiv(bcsub($decimal_i,$last),16);
		} while($decimal_i > 0);
		return $hex;
	}

	static function bin2hex($binary_i) {
		$val = unpack('H*',$binary_i);
		$trimmed = ltrim($val[1],'0');
		return $trimmed;
	}

	static function bin2dec($binary_i) {
		$ans = "0";
		$hex = self::bin2hex($binary_i);
		$hexa = "1";
		$orig = strtolower($hex);
		$valid = 1;
		for($i=(strlen($hex)-1);$i>=0;$i--) {
			$ans = bcadd($ans,(bcmul($hexa, base_convert(substr($hex,$i,1),16,10))));
			$hexa = bcmul($hexa,16);
		}
		return $ans;
	}

}
?>
