<?php
//Common includes for both web and other php services
set_include_path( get_include_path() . PATH_SEPARATOR . __DIR__ . PATH_SEPARATOR . __DIR__ . DIRECTORY_SEPARATOR . "libs" );
function __autoload($className) {
	$extensions = array(".php", ".class.php", ".inc");
	$paths = explode(PATH_SEPARATOR, get_include_path());
	$className = str_replace("_" , DIRECTORY_SEPARATOR, $className);
	foreach ($paths as $path) {
		$filenames[] = $path . DIRECTORY_SEPARATOR . $className;
		$filenames[] = $path . DIRECTORY_SEPARATOR . $className . DIRECTORY_SEPARATOR . $className;
		foreach ($extensions as $ext) {
			foreach($filenames as $filename) {
				$filename = str_replace("\\","/",$filename);
				if (is_readable($filename . $ext)) {
					require_once $filename . $ext;
					return;
					break;
				}
			}
		}
	}
}
?>
