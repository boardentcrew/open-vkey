<?php
	class controllers_Global {

		static function scope() {
			global $config;
			return array(
				"meta"=>array(
					"author"=>$config["author"],
					"title"=>$config["title"],
					"description"=>$config["description"]
				)
			);
		}

	}
?>
