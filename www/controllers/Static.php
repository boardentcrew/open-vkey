<?php
	class controllers_Static {

		static function front($params) {
			$config = Config::getInstance();
			echo Scope::getInstance()->
				add("config",$config)->
				render('front.php');
		}

		static function seeds($params) {
			$config = Config::getInstance();
			echo Scope::getInstance()->
				add("config",$config)->
				render('seeds.php');
		}

		static function test($params) {
			$config = Config::getInstance();
			echo Scope::getInstance()->
			add("config",$config)->
			render('test.php');
		}

		static function fetch($params) {
			$cache = Cache::getInstance();
			if (isset($params["vkey"])) {
				if ($g = $cache->get("vkey_"+$params["vkey"])) {
					echo json_encode($g);
					;
				} else {
					//Invalid vkey ( expired ? )
					echo json_encode(NULL);
				}
			}
		}

	}
?>
