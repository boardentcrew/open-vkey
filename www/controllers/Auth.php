<?php
	class controllers_auth {
		function main($params) {
			$requesting = (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '');

			session_start();

			if ($requesting) $_SESSION["origin"] = $requesting;

			if (isset($_COOKIE["openvkey"])) { //Session Nonce Exists
				//Check if exists in cache
				$cache = Cache::getInstance();
				if ($user = $cache->get("vkey_".$_COOKIE["openvkey"])) {
					echo "document.getElementById('openvkey').innerHTML = 'OVK Found : {$_COOKIE["openvkey"]}';\n";
					$uc = json_encode($_COOKIE["openvkey"]);
					echo "document.cookie = 'openvkey='+{$uc};\n";
					//echo "location.reload();\n";
					die();
				} else { //Session Nonce not found
					setcookie("openvkey","",time() - 3600);
					echo "document.cookie = 'openvkey=';\n";
				}
			}

			$window = "<B>We couldn't find your openVKey.</B><BR><a href=\"http://www.openvkey.com/test\" target=\"_BLANK\">Login via OpenVKey</a>";
			$window = json_encode($window);
			echo "document.getElementById('openvkey').innerHTML = {$window};\n";
			//echo "location.reload();\n";

		}
	}
?>