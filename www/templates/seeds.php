<?=$header?>
<script src="/assets/js/base58.js"></script>
<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha512.js"></script>
</script>

<BR><BR><BR><BR>
<div class='row'>
	<div class='col-sm-4'>
		<H2>Creating an Identity</H2>
		<p>First step to creating and Open Virtual Key identity is to generate a seed.</p>
		<P>A seed is a unique "fingerprint" to which your identity will be built around. When a seed is generated it is paired with an override key.</p>
	</div>
	<div class='col-sm-4'>
		<H2>The Override Key</H2>
		<P>In the event that you need to prove 100% ownership of this identity, you can do so with the override key. The override key was developed to give a person the 'last-say' in identity ownership. If your account becomes compromised, your are locked out, then you would use your override key to 'transfer' the account into a new seed.</P>
			
	</div>
	<div class='col-sm-4'>
		<H2>Completing your Identity</H2>		
		<P>Once you have an override key saved, and your seed is visible on the screen, then you can progress to the next section to complete the creation of your identity.</P>
	</div>
</div>
<BR>
<div class='row'>
	<div class='col-sm-6 text-center'>
		<B>Override</B><BR>
		<div id="override"></div>
	</div>

	<div class='col-sm-6 text-center'>
		<B>Seed</B><BR>
		<div id="seed"></div>
	</div>
</div>
<div class='row'>
	<div class='col-sm-12 text-center'>
		<div id='view'></div>
	</div>
</div>
<HR>
<BR>
<div class='col-sm-12 text-center'>
	<button class='button btn' onClick="genNew()"> Generate New Seed </button>
</div>
<BR><BR>


<script>
	function wordArrayToByteArray(wordArray) {
		// Shortcuts
		var words = wordArray.words;
		var sigBytes = wordArray.sigBytes;
		// Convert
		var u8 = new Uint8Array(sigBytes);
		for (var i = 0; i < sigBytes; i++) {
			var byte = (words[i >>> 2] >>> (24 - (i % 4) * 8)) & 0xff;
			u8[i]=byte;
		}
		return u8;
	}

	function genNew() {
		var override = CryptoJS.lib.WordArray.random( 64 );
		var seed = CryptoJS.SHA512( override );

		var bseed = bs58_encode( wordArrayToByteArray( seed ));
		var bgerm = bs58_encode( wordArrayToByteArray( override ));

		$('#seed').html( '<a href="/gen/'+bseed+'"> Generate Identity </a>');
		var marklet ='javascript:(function(){ovkOverride(\''+bgerm+'\');void(0)})();';
		$('#override').html('<p onClick=false >Drag <a class="bookmarklet" href="'+marklet+'">OVK Override</a> to your bookmark bar.</p>');
		$('#view').html('<B>'+bseed+'</B>');
	}

	function ovkOverride(param) {
		alert('Override does not work here. Please drag to bookmark for future use.');
	}
</script>
<?=$footer?>
