<!DOCTYPE html>
<html lang="en">
	<head>

		<?
			foreach($meta as $mk=>$mv) {
				if (is_array($mv)) {
					echo "<meta ";
					foreach($mv as $mmk=>$mmv) {
						echo "{$mmk}=\"{$mmv}\" ";
					}
					echo " />\n";
				} else {
					echo "<meta name=\"{$mk}\" content=\"{$mv}\">\n";
				}
			}
		?>

		<title><?= $title ?></title>

		<script type="text/javascript" src="/assets/js/jquery.min.js" ></script>
		<script type="text/javascript" src="/assets/js/bootstrap-switch.js" ></script>

		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-theme.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootswitch.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/custom.css">
	</head>
	<body>

		<nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"> Open vKey </a>
				</div>

				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav">
						<li><a href="/"> Home </a></li>
						<li><a href="/build"> Build </a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav>
		<div id="main-content" class="container content">
