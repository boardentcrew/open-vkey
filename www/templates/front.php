<?=$header?>

	<form class="form-signin" id="form" method="post" action="/">
		<h1 class="form-signin-heading text-muted">Sign In</h1>
		<div class="row">
			<div class="col-md-8">
				<input type="text" class="form-control" placeholder="Email address" required="" autofocus="">
				<input type="password" class="form-control" placeholder="Password" required="">
				<div class="pull-right">
					<label><input type="checkbox" class="switch" data-size="mini" value="1" name="remember"> Remember Me</label>
				</div>

				<div class="pull-left">
					<BR>
					Not Registred? <a href="/register">Register here</a>
				</div>

			</div>

			<div class="col-md-4">
				<div class="text-center">
					<img id="holder" class="img img-thumbnail" src="http://placehold.it/150x150&text=Drop+vKey+File">
					<textarea style="display:none" name="vkey" id="vkey"></textarea>
					<div id="status"></div>
				</div>
			</div>
		</div>
		<BR>
		<button class="btn btn-lg btn-primary btn-block" type="submit">
			Sign In
		</button>
	</form>

<script>
	$(document).ready(function() {
		$(".switch").bootstrapSwitch();
	});

	var holder = document.getElementById('holder'),
		state = document.getElementById('status');

	if (typeof window.FileReader === 'undefined') {
		state.className = 'fail';
	} else {
		state.className = 'success';
		state.innerHTML = 'Drag & Drop vKey';
	}

	holder.ondragover = function () { this.className = 'hover'; return false; };
	holder.ondragend = function () { this.className = ''; return false; };
	holder.ondrop = function (e) {
		this.className = '';
		e.preventDefault();

		var file = e.dataTransfer.files[0],
			reader = new FileReader();

		reader.onload = function (event) {
			$('#vkey').val(event.target.result);
			$('#form').submit();
		};

		reader.readAsDataURL(file);

		return false;
	};


</script>

<?=$footer?>