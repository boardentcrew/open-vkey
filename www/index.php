<?php
	//Define library path
	set_include_path( get_include_path() . PATH_SEPARATOR . __DIR__ . DIRECTORY_SEPARATOR . "libs" );

	//Initiate Autoloader
	include("autoload.php");

	//Load Configuration file
	Config::setFile('./config.php');
	$config = Config::getInstance();

	$cache = Cache::getInstance('default','localhost',11211);

	//Set template base path
	Scope::setBasePath($config["paths"]["templates"]);

	DB::set( $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] ); //Define DB Connection, use like : DB::getInstance()->prepare("SELECT * FROM ...

	$router = Router::getInstance();

	Scope::globals( 'title', $config["title"] );
	Scope::globals( 'meta', $config["meta"] );


	//Define filter routes here : ( used for non templated requests )
	$router->map("GET" , "/", "controllers_Static#front")->
			map("GET|POST", "/auth", "controllers_Auth#main","auth")->
		map("GET|POST", "/seeds", "controllers_Static#seeds","seeds")->
			map("GET|POST", "/test", "controllers_Static#test","test")->
			map("GET|POST", "/fetch/[*:vkey]", "controllers_Static#fetch","fetch")->
            map("GET|POST", "/build", "controllers_Build#main","build");

	$route = $router->match();

	if ($route == FALSE) {
		$route = array( //Assume 404
			"target"=>"controllers_Errors#notFound",
			"params"=>""
		);
	}

	try {
		if (isset($route)) $router->execute( $route );
	} catch ( Exception $e ) {
		var_dump($e);die();
		Scope::getInstance( array( "error" => $e->getMessage() ) )->render('error.php');
	}
?>
