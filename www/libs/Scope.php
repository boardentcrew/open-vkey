<?php

	/**
	* Scope class is used to build up key/value pairs to be accessed by another program/script. Scope is a small set of utils to use PHP as its own templating system.
	*
	*/
	
	class Scope {
		protected $vars; //Array of assigned values for scope

		static $globals=[];
		static $base_path='';
		

		function __construct($args) {
			if ( ! is_null($args) ) {
				foreach($args as $k=>$v) {
					$this->add($k,$v);
				}
			} else {
				$this->vars = array();
			}
		}


		function __toString() {
			return $this->export();
		}

		static function setBasePath($path=NULL) {
			if (is_null($path)) {
				self::$base_path = '';
			} else {
				self::$base_path = $path;
			}
		}
	
		public function export() {
			return serialize($this->vars);
		}

		static function getInstance($args=NULL) {
			return new Scope($args);
		}

		static function array_merge_recursive_distinct ( array &$array1, array &$array2 ) {
			$merged = $array1;

			foreach ( $array2 as $key => &$value )
			{
				if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
				{
					$merged [$key] = self::array_merge_recursive_distinct ( $merged [$key], $value );
				}
				else
				{
					$merged [$key] = $value;
				}
			}
			return $merged;
		}

		public function add($label,$value,$additive=FALSE) {
			if ($additive===TRUE & isset($this->vars[$label])) {
				if (! is_array($this->vars[$label])) {
					$this->vars[$label] = array($this->vars[$label]);
	
				}
				$this->vars[$label][] = $value;
			} else {
				$this->vars[$label] = $value;
			}
			return $this;
		}
		
		static function globals($label,$value=NULL) {
			if (is_null($value) && is_array($label)) {
				foreach($label as $k=>$v) {
					self::$globals[$k]=$v;
				}
			}
			self::$globals[$label] = $value;
		}

		public function render($template=NULL,$headerfooter=true) {
			$output = '';
			$template = self::$base_path.$template;
			if (is_readable($template)) {

				$vars = self::array_merge_recursive_distinct ( static::$globals, $this->vars );

				if ($headerfooter) {
					$vars['header'] = Scope::getInstance( $vars )->render('header.php',false);
					$vars['footer'] = Scope::getInstance( $vars )->render('footer.php',false);
				}


				extract( $vars ,EXTR_REFS );
				ob_start();
				try {
					include_once($template);
				} catch (Exception $e) {
					ob_end_clean();
					throw new Exception("Unable to render template file '{$template}' : ".$e->getMessage());
				}
				$output = ob_get_contents();
				ob_end_clean();
				return $output;
			} else {
				throw new Exception("Unable to read template file : {$template}");
			}
			return false;
		}

	}
?>
