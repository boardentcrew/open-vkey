<?php
	class Cache {
		static $instances;
		static $current_instance='default';

		static function getInstance($name=NULL,$host=NULL,$port=NULL) {
			if ( is_null($name)) {
				$name = static::$current_instance;
			} else {
				static::$current_instance = $name;
				if (!is_null($host)) {
					static::$instances[$name] = new Memcache;
					static::$instances[$name]->connect($host,$port);
				}
			}

			if (!isset(static::$instances[$name])) {
				return false;
			} else {
				return static::$instances[$name];
			}

		}
	}
?>