<?php
/**
 * Config File to be used with Config Singleton
 *
 * Makes for highly portable configuration file
 *
 */

	$config = array();

	$config["title"] = "Open vKey - Your badge to the Internet";

	$config["meta"] = array();

	$config["meta"][] = array(
		"charset"=>"utf-8"
	);

	$config["meta"][] = array(
		"name"=>"viewport",
		"content"=>"width=device-width, initial-scale=1.0"
	);

	$config["meta"]["author"] = "Danjelo Morgaux";
	$config["meta"]["description"] = "Build your identity here";

	$config["paths"]["templates"] = __DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR;

	$config["db"]["dsn"] = "mysql:host=localhost;dbname=clipgif";
	$config["db"]["username"] = "clipgif";
	$config["db"]["password"] = "sEYWDqrKMW7TMGHS";

	return $config;
?>
