<?php
//OVK - Facade for Open Virtual Key Functions
include_once(__DIR__."/Badge.php");
include_once(__DIR__."/ECC.php"); //Elliptical Curve Crypto
class OVK {
	static public function newECC($curveType=NULL) { //ECC Factory
		return new \OVK\ECC($curveType);
	}
	static public function newBadge($overridePublic) {
		return \OVK\Badge::create($overridePublic);
	}

	static public function encode58($addr) {
		$num = gmp_strval(gmp_init($addr, 16), 58);
		$num = strtr($num, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv', '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');
		return $num;
	}
	static public function decode58($addr) {
		$deshift = strtr($addr, '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz', '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv' );
		$num = gmp_strval(gmp_init($deshift, 58),16);

		return str_pad($num,(strlen($num)%2?strlen($num)+1:strlen($num)),'0', STR_PAD_LEFT);
	}
}
?>
