<?php namespace OVK;
class Badge {
	var $badge;
	private $identPrivates;
	var $identPublics;
	var $overridePublics;
	var $address;
	var $type;

	const ADDRESS_IDENTITY=1;
	const ADDRESS_SERVICE=2;
	const ADDRESS_STAMP=3;

	function __construct($address=NULL,$badge=NULL) {
		if (!is_null($address) and !is_null($badge)) { //Load from badge
			$hexBadge = \OVK::decode58($badge);
			$addressParts = self::deAddress($address);
			$this->badge($hexBadge,$addressParts[1]);
			if ($this->address != $address) return false; //Not valid
		}
	}

	//OverridePublic is the X coord of the override public key.
	//type is the type of badge/address being rendered
	static function create($overridePublic,$type=self::ADDRESS_IDENTITY) {
		$badge = new self();
		$badge->identPrivates();
		$hexOverride = \OVK::decode58($overridePublic);
		$badge->badge($hexOverride.$badge->identPublics[0],$type);
		return $badge;
	}

	public function identPrivates($points=NULL) {
		$ecc = \OVK::newECC();
		if (is_null($points)) $points = $ecc->privatePoints();
		$this->identPrivates = $points;
		$this->identPublics = $ecc->publicPoints(array($this->identPrivates[0],$this->identPrivates[1]));
	}

	private function badge($newBadge,$type=self::ADDRESS_IDENTITY) {
		$this->type = $type;
		$ecc = \OVK::newEcc();
		$this->badge = \OVK::encode58($newBadge);
		$this->address = self::address($newBadge,$type);
		if (!$this->identPublics) {
			$ident = substr($newBadge,64,64);
			$identY = $ecc->YfromX($ident);
			$this->identPublics[0] = $ident;
			$this->identPublics[1] = gmp_strval($identY[1],16);
		}
		if (!$this->overridePublics) {
			$override = substr($newBadge,0,64);
			$overrideY = $ecc->YfromX($override);
			$this->overridePublics[0] = $override;
			$this->overridePublics[1] = gmp_strval($overrideY[1],16);
		}
	}

	static public function address($hash,$addressType=self::ADDRESS_IDENTITY) {
		$prefix = '00';
		switch ($addressType) {
			default:
			case self::ADDRESS_IDENTITY : $prefix = '01'; break;
			case self::ADDRESS_SERVICE : $prefix = '02'; break;
			case self::ADDRESS_STAMP : $prefix = '03'; break;
		}
		return \OVK::encode58($prefix.hash('sha256',hash('sha256', $hash)));
	}

	static public function deAddress($address) {
		$raw = \OVK::decode58($address);
		$type = NULL;
		$prefix = substr($raw,0,2);
		switch($prefix) {
			default :
			case '01' : $type = self::ADDRESS_IDENTITY; break;
			case '02' : $type = self::ADDRESS_SERVICE; break;
			case '03' : $type = self::ADDRESS_STAMP; break;
		}
		$rawAddress = substr($raw,2);
		return array($rawAddress,$type);
	}
}
