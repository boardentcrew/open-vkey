<?php
namespace OVK;
//Code from https://raw.githubusercontent.com/rgex/BitcoinECDSA.php/master/src/BitcoinPHP/BitcoinECDSA/BitcoinECDSA.php
class ECC {
	public $curve;
	public $gen;

	private $prime;
	private $order;
	private $alpha;
	private $beta;
	private $g;

	const CURVE_SECP256K1=1;
	const CURVE_SECP256R1=2;

	function __construct($curveType = NULL) {
		$this->buildCurve($curveType);
	}

	function buildCurve($curveType) {
		$prime = $alpha = $beta = $order = $genX = $genY = 0;
		//Init Curve
		switch ($curveType) {
			default:
			case self::CURVE_SECP256K1 :
				$this->prime = gmp_init('FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F', 16);
				$this->order = gmp_init('FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141', 16);

				//Coefficients
				$this->alpha = 0;
				$this->beta = gmp_init('0000000000000000000000000000000000000000000000000000000000000007', 16);

				//Point G
				$this->g['x'] = gmp_init('79BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798', 16);
				$this->g['y'] = gmp_init('483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8', 16);
				break;
		}
		//Build Curve and Points
		$this->curve = new \OVK\Curve($this->prime, $this->alpha, $this->beta);
		$this->gen = new \OVK\Point($this->curve, $this->g['x'], $this->g['y'], $this->order);
	}

	public function sqrt($a) {
		$p = $this->prime;

		if(gmp_legendre($a, $p) != 1) {
			//no result
			return null;
		}

		if(gmp_strval(gmp_mod($p, gmp_init(4, 10)), 10) == 3) {
			$sqrt1 = gmp_powm( $a, gmp_div_q( gmp_add($p, gmp_init(1, 10)), gmp_init(4, 10)), $p);
			// there are always 2 results for a square root
			// In an infinite number field you have -2^2 = 2^2 = 4
			// In a finite number field you have a^2 = (p-a)^2
			$sqrt2 = gmp_mod(gmp_sub($p, $sqrt1), $p);
			return array($sqrt1, $sqrt2);
		} else {
			throw new \Exception('P % 4 != 3 , this isn\'t supported yet.');
		}
	}

	public function YfromX($x, $derEvenOrOddCode = null) {
		$a  = $this->alpha;
		$b  = $this->beta;
		$p  = $this->prime;

		$x  = gmp_init($x, 16);
		$y2 = gmp_mod( gmp_add( gmp_add( gmp_powm($x, gmp_init(3, 10), $p), gmp_mul($a, $x)), $b), $p);

		$y = $this->sqrt($y2);

		if(!$y) {
			return null;
		}

		if(!$derEvenOrOddCode) {
			return $y;
		} else if($derEvenOrOddCode == '02') {
			$resY = null;
			if(false == gmp_strval(gmp_mod($y[0], gmp_init(2, 10)), 10)) $resY = gmp_strval($y[0], 16);
			if(false == gmp_strval(gmp_mod($y[1], gmp_init(2, 10)), 10)) $resY = gmp_strval($y[1], 16);
			if($resY) while(strlen($resY) < 64) $resY = '0' . $resY;
			return $resY;
		} else if($derEvenOrOddCode == '03') {
			$resY = null;
			if(true == gmp_strval(gmp_mod($y[0], gmp_init(2, 10)), 10)) $resY = gmp_strval($y[0], 16);
			if(true == gmp_strval(gmp_mod($y[1], gmp_init(2, 10)), 10)) $resY = gmp_strval($y[1], 16);
			if($resY) while(strlen($resY) < 64) $resY = '0' . $resY;
			return $resY;
		}
		return null;
	}

	static public function hash256($in) {
		return hash('sha256',hash('sha256', $in));
	}

	static public function randomBytes($length) {
		$out = bin2hex(openssl_random_pseudo_bytes($length,$strong));
		return $out;
	}

	public function privatePoints() { //Generate a new private point
		$privateX = hash('sha256',self::randomBytes(32));
		while(is_null($privatePoints = $this->YFromX($privateX))) $privateX = hash('sha256',self::randomBytes(32));
		$privatePoints = array($privateX,gmp_strval($privatePoints[0],16));
		return $privatePoints;
	}

	function publicPoints($privatePoints) { //Generate public point from private points
		$z = self::pointsToKey($privatePoints);
		$pubPoint = \OVK\Point::mul(gmp_init($z,16),$this->gen);
		return array(gmp_strval($pubPoint->x,16),gmp_strval($pubPoint->y,16));
	}

	function signPoints($privateH,$hash,$nonce=NULL) {
		$privateY = $this->YfromX($privateH);
		$k = gmp_strval($privateH,16).gmp_strval($privateY[0],16);
		if (is_null($nonce)) {
			$nonce = self::nonce();
		}
		$rPt = \OVK\Point::mul(gmp_init($nonce,16),$this->gen);
		$R = gmp_strval($rPt->x,16);
		while(strlen($R) < 64) $R = '0'.$R;
		$S = gmp_strval(gmp_mod(gmp_mul(gmp_invert( gmp_init( $nonce, 16 ), $this->order),gmp_add(gmp_init($hash,16),gmp_mul(gmp_init($k,16),gmp_init($R,16)))),$this->order),16);
		if (strlen($S)%2) $S = '0'.$S;
		if (strlen($R)%2) $R = '0'.$R;
		var_dump(array($R,$S));
		return array($R,$S);
	}

	function checkSignPoints($public,$bsig,$hash) {
		$publicY = $this->YfromX($public);
		$publicPoints = array($public,gmp_strval($publicY[0],16));

		$sig = \OVK::decode58($bsig);
		$R = substr($sig,0,64);
		$S = substr($sig,64,64);

		// S^-1* hash * G + S^-1 * R * Qa
		// S^-1* hash
		$exp1 =  gmp_mul( gmp_invert( gmp_init($S, 16), $this->order ), gmp_init($hash, 16) );

		// S^-1* hash * G
		$exp1Point = \OVK\Point::mul($exp1,$this->gen);

		// S^-1 * R
		$exp2 =  gmp_mul( gmp_invert( gmp_init($S, 16), $this->order ), gmp_init($R, 16) );
		// S^-1 * R * Qa
		$pp = new \OVK\Point($this->curve,gmp_init($publicPoints[0],16),gmp_init($publicPoints[1],16));
		$exp2Point = \OVK\Point::mul($exp2, $pp);
		$resultPoint = \OVK\Point::add($exp1Point,$exp2Point);
		$xRes = str_pad(gmp_strval($resultPoint->x, 16),64,'0',STR_PAD_LEFT);
		var_dump($xRes);
		var_dump($R);

		if(strtoupper($xRes) == strtoupper($R))
			return true;
		else
			return false;
	}

	static function pointsToKey($points) {
		return join($points,'');
	}

	static function keyToPoints($key) {
		$x = gmp_init(substr($key, 0, 64), 16);
		$y = gmp_init(substr($key, 64, 64), 16);
		return array($x,$y);
	}

	function nonce() {
		$random = openssl_random_pseudo_bytes(256,$strong);
		$nonce = gmp_strval(gmp_mod(gmp_init(hash('sha256',$random),16),$this->order),16);
		return $nonce;
	}

	function mpk($mpk, $index=0, $change = false) {
		// prepare the input values
		$x = gmp_init(substr($mpk, 0, 64), 16);
		$y = gmp_init(substr($mpk, 64, 64), 16);
		$branch = $change ? 1 : 0;
		$z = gmp_init(hash('sha256', hash('sha256', "$index:$branch:" . pack('H*', $mpk), TRUE)), 16);

		// generate the new public key based off master and sequence points
		try {
			$pt = \OVK\Point::add(new Point($this->curve, $x, $y), \OVK\Point::mul($z, $this->gen));
		} catch ( Exception $e ) {
			return false;
		}
		$keystr = pack('H*', '04'
			. str_pad(gmp_strval($pt->x, 16), 64, '0', STR_PAD_LEFT)
			. str_pad(gmp_strval($pt->y, 16), 64, '0', STR_PAD_LEFT));
		$vh160 =  '00' . hash('ripemd160', hash('sha256', $keystr, TRUE));
		$addr = $vh160 . substr(hash('sha256', hash('sha256', pack('H*', $vh160), TRUE)), 0, 8);

		return $addr;
	}
}

class Curve {

	public function __construct($prime, $a, $b) {
		$this->a = $a;
		$this->b = $b;
		$this->prime = $prime;
	}

	public function contains($x, $y) {
		return !gmp_cmp(gmp_mod(gmp_sub(gmp_pow($y, 2), gmp_add(gmp_add(gmp_pow($x, 3), gmp_mul($this->a, $x)), $this->b)), $this->prime), 0);
	}

	public static function cmp(Curve $cp1, Curve $cp2) {
		return gmp_cmp($cp1->a, $cp2->a) || gmp_cmp($cp1->b, $cp2->b) || gmp_cmp($cp1->prime, $cp2->prime);
	}
}

class Point {

	const INFINITY = 'infinity';

	public function __construct(Curve $curve, $x, $y, $order = null) {
		$this->curve = $curve;
		$this->x = $x;
		$this->y = $y;
		$this->order = $order;

		if (isset($this->curve) && ($this->curve instanceof Curve)) {
			if (!$this->curve->contains($this->x, $this->y)) {
				throw new \ErrorException('Curve does not contain point');
			}

			if ($this->order != null) {
				if (self::cmp(self::mul($order, $this), self::INFINITY) != 0) {
					throw new \ErrorException('Self*Order must equal infinity');
				}
			}
		}
	}

	public static function cmp($p1, $p2) {
		if (!($p1 instanceof Point)) {
			if (($p2 instanceof Point)) return 1;
			if (!($p2 instanceof Point)) return 0;
		}
		if (!($p2 instanceof Point)) {
			if (($p1 instanceof Point)) return 1;
			if (!($p1 instanceof Point)) return 0;
		}
		return gmp_cmp($p1->x, $p2->x) || gmp_cmp($p1->y, $p2->y) || \OVK\Curve::cmp($p1->curve, $p2->curve);
	}

	public static function add($p1, $p2) {

		if (self::cmp($p2, self::INFINITY) == 0 && ($p1 instanceof Point)) {
			return $p1;
		}
		if (self::cmp($p1, self::INFINITY) == 0 && ($p2 instanceof Point)) {
			return $p2;
		}
		if (self::cmp($p1, self::INFINITY) == 0 && self::cmp($p2, self::INFINITY) == 0) {
			return self::INFINITY;
		}

		if (\OVK\Curve::cmp($p1->curve, $p2->curve) == 0) {
			if (gmp_cmp($p1->x, $p2->x) == 0) {
				if (gmp_mod(gmp_add($p1->y, $p2->y), $p1->curve->prime) == 0) {
					return self::INFINITY;
				} else {
					return self::double($p1);
				}
			}

			$p = $p1->curve->prime;
			$l = gmp_mul(gmp_sub($p2->y, $p1->y), gmp_invert(gmp_sub($p2->x, $p1->x), $p));
			$x3 = gmp_mod(gmp_sub(gmp_sub(gmp_pow($l, 2), $p1->x), $p2->x), $p);
			$y3 = gmp_mod(gmp_sub(gmp_mul($l, gmp_sub($p1->x, $x3)), $p1->y), $p);
			return new Point($p1->curve, $x3, $y3);
		} else {
			throw new ErrorException('Elliptic curves do not match');
		}
	}

	public static function mul($x2, Point $p1) {
		$e = $x2;
		if (self::cmp($p1, self::INFINITY) == 0) {
			return self::INFINITY;
		}
		if ($p1->order != null) {
			$e = gmp_mod($e, $p1->order);
		}
		if (gmp_cmp($e, 0) == 0) {
			return self::INFINITY;
		}
		if (gmp_cmp($e, 0) > 0) {
			$e3 = gmp_mul(3, $e);
			$negative_self = new Point($p1->curve, $p1->x, gmp_neg($p1->y), $p1->order);
			$i = gmp_div(self::leftmost_bit($e3), 2);
			$result = $p1;
			while (gmp_cmp($i, 1) > 0) {
				$result = self::double($result);
				if (gmp_cmp(gmp_and($e3, $i), 0) != 0 && gmp_cmp(gmp_and($e, $i), 0) == 0) {
					$result = self::add($result, $p1);
				}
				if (gmp_cmp(gmp_and($e3, $i), 0) == 0 && gmp_cmp(gmp_and($e, $i), 0) != 0) {
					$result = self::add($result, $negative_self);
				}
				$i = gmp_div($i, 2);
			}
			return $result;
		}
	}

	public static function leftmost_bit($x) {
		if (gmp_cmp($x, 0) > 0) {
			$result = 1;
			while (gmp_cmp($result, $x) <= 0) {
				$result = gmp_mul(2, $result);
			}
			return gmp_div($result, 2);
		}
	}

	public static function double(Point $p1) {
		$p = $p1->curve->prime;
		$a = $p1->curve->a;
		$inverse = gmp_invert(gmp_mul(2, $p1->y), $p);
		$three_x2 = gmp_mul(3, gmp_pow($p1->x, 2));
		$l = gmp_mod(gmp_mul(gmp_add($three_x2, $a), $inverse), $p);
		$x3 = gmp_mod(gmp_sub(gmp_pow($l, 2), gmp_mul(2, $p1->x)), $p);
		$y3 = gmp_mod(gmp_sub(gmp_mul($l, gmp_sub($p1->x, $x3)), $p1->y), $p);
		if (gmp_cmp(0, $y3) > 0) $y3 = gmp_add($p, $y3);
		return new Point($p1->curve, $x3, $y3);
	}
}
?>
