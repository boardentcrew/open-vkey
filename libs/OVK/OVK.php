<?php
//OVK - Facade for Open Virtual Key Functions
class OVK {
	static public function newPair() {
		$keyRes = openssl_pkey_new(array(
			'private_key_bits'=>512
		));
		openssl_pkey_export($keyRes, $privatePem);
		$privateBin = self::pem2bin($privatePem);
		$keyInfo = openssl_pkey_get_details($keyRes);
		$publicPem = $keyInfo['key'];
		$publicBin = self::pem2bin($publicPem);
		return array(bin2hex($privateBin),bin2hex($publicBin));
	}

	static public function address($badge,$type='01') {
		return $type.substr(hash('sha256', hash('sha256', $badge)),0,62);
	}

	public static function pem2bin($pem) {
		$pemParts = split("\n",$pem);
		$dechunked = "";
		foreach($pemParts as $line) if (substr($line,0,4) != "----") $dechunked .= trim($line);
		return base64_decode($dechunked);
	}
	public static function bin2pem($bin,$private=false) {
		if ($private) {
			return "-----BEGIN PRIVATE KEY-----\n".chunk_split(base64_encode($bin),64,"\n")."-----END PRIVATE KEY-----\n";
		} else {
			return "-----BEGIN PUBLIC KEY-----\n".chunk_split(base64_encode($bin),64,"\n")."-----END PUBLIC KEY-----\n";
		}
	}

	static public function encode58($addr) {
		$num = gmp_strval(gmp_init($addr, 16), 58);
		$num = strtr($num, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv', '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz');
		return $num;
	}
	static public function decode58($addr) {
		$deshift = strtr($addr, '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz', '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv' );
		$num = gmp_strval(gmp_init($deshift, 58),16);
		return str_pad($num,(strlen($num)%2?strlen($num)+1:strlen($num)),'0', STR_PAD_LEFT);
	}
}
?>
